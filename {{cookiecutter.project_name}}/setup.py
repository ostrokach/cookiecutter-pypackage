from setuptools import find_packages, setup

{%- set license_classifiers = {
    "MIT license": "License :: OSI Approved :: MIT License",
    "BSD license": "License :: OSI Approved :: BSD License",
    "ISC license": "License :: OSI Approved :: ISC License (ISCL)",
    "Apache Software License 2.0": "License :: OSI Approved :: Apache Software License",
    "GNU General Public License v3": "License :: OSI Approved :: "
    "GNU General Public License v3 (GPLv3)"
} %}


def read_file(file):
    with open(file) as fin:
        return fin.read()


setup(
    name="{{ cookiecutter.project_name }}",
    version="{{ cookiecutter.version }}",
    description="{{ cookiecutter.project_short_description }}",
    long_description=read_file("README.md"),
    long_description_content_type="text/markdown",
    author="{{ cookiecutter.full_name.replace("\"", "\\\"") }}",
    author_email="{{ cookiecutter.email }}",
    url="https://gitlab.com/{{ cookiecutter.git_username }}/{{ cookiecutter.project_name }}",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    package_data={},
    include_package_data=True,
{%- if cookiecutter.open_source_license in license_classifiers %}
    license="{{ cookiecutter.open_source_license }}",
{%- endif %}
    zip_safe=False,
    keywords="{{ cookiecutter.project_slug }}",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
{%- if cookiecutter.open_source_license in license_classifiers %}
        "{{ license_classifiers[cookiecutter.open_source_license] }}",
{%- endif %}
        "Natural Language :: English",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    test_suite="tests",
)
