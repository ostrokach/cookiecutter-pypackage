========
Contents
========

.. toctree::
   :caption: Overview
   :name: mastertoc
   :maxdepth: 2

   readme
   installation
   usage
   contributing
{% if cookiecutter.create_author_file == 'y' %}   authors{% endif %}

.. toctree::
   :caption: Modules
   :name: modules
   :maxdepth: 3

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
