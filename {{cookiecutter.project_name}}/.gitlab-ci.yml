default:
  image: condaforge/linux-anvil-cos7-x86_64:latest

stages:
  - custom
  - lint
  - build
  - test
  - doc
  - deploy

# === Variables ===

variables:
  PACKAGE_VERSION: "{{ cookiecutter.version }}"
  PYTHON_VERSION: "3.9"

# === Configurations ===

.skip-custom-pipelines:
  except:
    variables:
      - $UPDATE_PAGES
      - $BUILD_IMAGE

.configure:
  extends:
    - .skip-custom-pipelines
  before_script:
    # Set conda envs and pkgs dirs
    - |
      cat <<EOF > ~/.condarc
      channel_priority: true
      channels:
        - pytorch
        - conda-forge
        - defaults
        - kimlab
        - {{ cookiecutter.anaconda_username }}
        - bioconda
        - salilab
        - omnia
      EOF
    - conda install -y -q mamba

# === Lint ===

lint:
  stage: lint
  extends:
    - .configure
  script:
    - mamba create -n lint -q "python=${PYTHON_VERSION}" isort toml flake8 mypy black
    - source activate lint
    - python -m isort -p ${CI_PROJECT_NAME} -c .
    - python -m flake8 src
    - python -m black --config pyproject.toml --check .
    # MyPy does not support namespace packages until this issue gets resolved:
    # https://github.com/python/mypy/issues/1645
    - python -m mypy src/${CI_PROJECT_NAME} || true

# === Build ===

build:
  stage: build
  extends:
    - .configure
  script:
    - mamba install -yq conda conda-build conda-verify conda-forge-pinning
    - cd "${CI_PROJECT_DIR}/devtools/conda"
    - >
      mamba build .
      --variant-config-files /opt/conda/conda_build_config.yaml
      --variants "{python: [$PYTHON_VERSION], numpy: [1.16], python_impl: [cpython]}"
      --no-test
      --output-folder "$CI_PROJECT_DIR/conda-bld"
  artifacts:
    paths:
      - conda-bld

# === Test ===

test:
  stage: test
  extends:
    - .configure
  script:
    # Create conda environment for testing
    - mamba create -n test -q -c file://${CI_PROJECT_DIR}/conda-bld --strict-channel-priority
      "python=${PYTHON_VERSION}" ${CI_PROJECT_NAME} pytest pytest-cov || true
    - source activate test
    # Run tests
    - PKG_INSTALL_DIR=$(python -c "import {{ cookiecutter.project_import }}; print({{ cookiecutter.project_import }}.__path__[0])")
    - python -m pytest
      -c pyproject.toml
      --cov="${PKG_INSTALL_DIR}"
      --cov-config=pyproject.toml
      --color=yes
      "tests/"
    # Coverage
    - mkdir coverage
    - mv .coverage coverage/.coverage.all
  artifacts:
    paths:
      - coverage
  dependencies:
    - build

# === Document ===

# NB: Has to be called "docs" for the pages script to work.
docs:
  stage: doc
  extends:
    - .configure
  script:
    # Create conda environment for testing
    - conda update -yq conda
    - conda create -n test -q -c file://${CI_PROJECT_DIR}/conda-bld --strict-channel-priority
      "python=${PYTHON_VERSION}" ${CI_PROJECT_NAME} nbconvert ipython ipykernel pandoc || true
    - source activate test
    - pip install -q 'sphinx>=3.4' sphinx_rtd_theme msmb_theme nbsphinx coverage toml
      'recommonmark>=0.5' sphinx-markdown-tables
    # Build docs
    - sphinx-build ${CI_PROJECT_DIR}/docs public
    - ln -s . public/docs
    # Coverage
    - coverage combine coverage/
    - coverage report
    - coverage html
    - mv htmlcov public/
  coverage: /^TOTAL.* (\d+\%)/
  dependencies:
    - build
    - test
  artifacts:
    paths:
      - public
    when: always

# === Deploy ===

deploy:
  stage: deploy
  extends:
    - .configure
  script:
    - anaconda -t $ANACONDA_TOKEN upload $CI_PROJECT_DIR/*/*/*.tar.bz2 -u {{ cookiecutter.anaconda_username }} --no-progress --force
  only:
    - tags
  dependencies:
    - build
  allow_failure: true # TODO: Change when no longer testing.

deploy-pypi:
  stage: deploy
  extends:
    - .configure
  script:
    - python -m pip install -q twine wheel
    - python setup.py sdist bdist_wheel
    - twine upload dist/*
  only:
    - tags
  allow_failure: true # TODO: Change when no longer testing.

trigger-custom-pipelines:
  stage: deploy
  extends:
    - .skip-custom-pipelines
  image:
    name: ubuntu:20.04
  before_script:
    - apt-get -y -qq update
    - apt-get -y -qq install curl jq
  script:
    - >
      BUILD_JOB_ID=$( \
        curl --globoff -sS --header "PRIVATE-TOKEN: ${GITLAB_CI_TOKEN}" \
          "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs?scope[]=success" \
        | jq '.[] | select(.name == "build") | .id' \
      )
    # Update docker image
    - curl --request POST
      --form token="${CI_JOB_TOKEN}"
      --form ref=${CI_COMMIT_TAG}
      --form "variables[BUILD_IMAGE]=true"
      --form "variables[BUILD_JOB_ID]=${BUILD_JOB_ID}"
      https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/trigger/pipeline
    # Update pages
    - curl --request POST
      --form token="${CI_JOB_TOKEN}"
      --form ref=${CI_COMMIT_TAG}
      --form "variables[UPDATE_PAGES]=true"
      https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/trigger/pipeline
  only:
    - tags

# === Custom pipelines ===

pages:
  stage: custom
  before_script:
    - pip install gitlab_versioned_pages
  script:
    - mkdir -p ./public
    - python -m gitlab_versioned_pages
      --project-id ${CI_PROJECT_ID}
      --job-name docs
      --private-token ${CI_DOCS_TOKEN}
      --output-dir ./public
      --url "https://${CI_PROJECT_NAMESPACE}.gitlab.io"
  artifacts:
    paths:
      - public
  only:
    variables:
      - $UPDATE_PAGES

docker:
  stage: custom
  image:
    name: docker:latest
  services:
    - docker:dind
  script:
    # Create tag
    - if [[ ! -z ${CI_COMMIT_TAG} ]] ; then
      IMAGE_TAG=${CI_COMMIT_TAG} ;
      else
      IMAGE_TAG="latest" ;
      fi
    - echo "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}"
    # Create artifact url
    - if [[ ! -z ${BUILD_JOB_ID} ]] ; then
      export CONDA_BLD_ARCHIVE_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs/${BUILD_JOB_ID}/artifacts" ;
      else
      export CONDA_BLD_ARCHIVE_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs/artifacts/master/download?job=build" ;
      fi
    - echo ${CONDA_BLD_ARCHIVE_URL}
    # Build Docker image
    - docker login registry.gitlab.com -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"
    - >
      docker build
      --build-arg CONDA_BLD_ARCHIVE_URL
      --build-arg CONDA_BLD_REQUEST_HEADER="PRIVATE-TOKEN: ${GITLAB_CI_TOKEN}"
      -t "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}"
      devtools/docker/
    - docker push "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}"
  only:
    variables:
      - $BUILD_IMAGE
