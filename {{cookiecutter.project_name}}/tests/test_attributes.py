import pytest

import {{ cookiecutter.project_import }}


@pytest.mark.parametrize("attribute", ["__version__"])
def test_attribute(attribute):
    assert getattr({{ cookiecutter.project_import }}, attribute)


def test_main():
    import {{ cookiecutter.project_import }}

    assert {{ cookiecutter.project_import }}
