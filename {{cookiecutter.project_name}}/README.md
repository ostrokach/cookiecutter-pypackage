{% set is_open_source = cookiecutter.open_source_license != 'Not open source' -%}
# {{ cookiecutter.project_name }}

[![gitlab](https://img.shields.io/badge/GitLab-main-orange?logo=gitlab)](https://gitlab.com/{{ cookiecutter.git_username }}/{{ cookiecutter.project_name }})
{% if is_open_source -%}
[![docs](https://img.shields.io/badge/docs-v{{ cookiecutter.version }}-blue.svg?logo=gitbook)](https://{{ cookiecutter.git_username }}.gitlab.io/{{ cookiecutter.project_name }}/v{{ cookiecutter.version }}/)
[![conda](https://img.shields.io/conda/dn/{{ cookiecutter.anaconda_username }}/{{ cookiecutter.project_name }}.svg?logo=conda-forge)](https://anaconda.org/{{ cookiecutter.anaconda_username }}/{{ cookiecutter.project_name }}/)
{%- endif %}
[![pipeline status](https://gitlab.com/{{ cookiecutter.git_username }}/{{ cookiecutter.project_name }}/badges/v{{ cookiecutter.version }}/pipeline.svg)](https://gitlab.com/{{ cookiecutter.git_username }}/{{ cookiecutter.project_name }}/commits/v{{ cookiecutter.version }}/)
[![coverage report](https://gitlab.com/{{ cookiecutter.git_username }}/{{ cookiecutter.project_name }}/badges/v{{ cookiecutter.version }}/coverage.svg?job=docs)](https://{{ cookiecutter.git_username }}.gitlab.io/{{ cookiecutter.project_name }}/v{{ cookiecutter.version }}/htmlcov/)

{{ cookiecutter.project_short_description }}
