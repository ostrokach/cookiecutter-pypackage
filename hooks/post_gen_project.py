#!/usr/bin/env python
import os
import os.path as op

PROJECT_DIRECTORY = op.realpath(op.curdir)


def remove_file(filepath):
    os.remove(op.join(PROJECT_DIRECTORY, filepath))


def create_symbolic_links():
    for filename in ["README.md"]:
        try:
            os.unlink(op.join(PROJECT_DIRECTORY, "docs", filename.lower()))
        except FileNotFoundError:
            pass
        os.symlink("../{}".format(filename), op.join(PROJECT_DIRECTORY, "docs", filename.lower()))


def create_init_file():
    init_file_path = op.join(
        PROJECT_DIRECTORY, "{{ cookiecutter.project_import.replace('.', '/') }}"
    )
    os.makedirs(init_file_path, exist_ok=True)

    init_file = op.join(init_file_path, "__init__.py")
    if not op.isfile(init_file):
        with open(init_file, "wt") as fout:
            fout.write('__version__ = "{{ cookiecutter.version }}"\n')


if __name__ == "__main__":
    # create_init_file()
    create_symbolic_links()

    if "{{ cookiecutter.create_author_file }}" != "y":
        remove_file("docs/authors.rst")

    if "Not open source" == "{{ cookiecutter.open_source_license }}":
        remove_file("LICENSE")
