# Cookiecutter template for a Python package

## Features

- Adds a GitLab CI / CD config.
- Native support for building and testing using the `conda` package manager.
- Generate multi-version documentation using [`ostrokach/gitlab-versioned-pages`](https://gitlab.com/ostrokach/gitlab-versioned-pages).
